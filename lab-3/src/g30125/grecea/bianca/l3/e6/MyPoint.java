package g30125.grecea.bianca.l3.e6;

public class MyPoint {
    //Two instance variables x (int) and y (int).
	private int x;
	private int y;
	//A �no-argument� (or �no-arg�) constructor that construct a point at (0, 0).
	MyPoint(){
		x=0;
		y=0;
	}
	//A constructor that constructs a point with the given x and y coordinates.
	MyPoint(int x1, int y1) {
		x=x1;
		y=y1;
	}
	//Getter and setter for the instance variables x and y.
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	//A method setXY() to set both x and y.
	public void setXY(int x1,int y1) {
		x=x1;
		y=y1;
	}
	//A toString() method that returns a string description of the instance in the format �(x, y)�.
	public void sayInfo() {
		System.out.println("("+x+","+y+")");
	}
	//A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
	public double distance(int x,int y) {
		return Math.sqrt(Math.pow(getX()-x,2)+Math.pow(getY()-y,2));
	}
	//An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
	public double distance(MyPoint p) {
		return Math.sqrt(Math.pow(p.x-x,2)+Math.pow(p.y-y,2));
	}
}
