package g30125.grecea.bianca.l3.e6;

public class TestMyPoint {
	public static void main(String[] args) {
		//construct at 0,0
		MyPoint point1=new MyPoint();
		//construct at given x,y
		MyPoint point2=new MyPoint(3,5);
		
		point1.sayInfo();
		point2.sayInfo();
		
		//getX, getY
		System.out.println("X-ul primului punct este:"+point1.getX());
		System.out.println("Y-ul celui de al doilea punct este:"+point2.getY());
		
		//setX,setY
		point1.setX(2);
		point2.setY(6);
		
		point1.sayInfo();
		point2.sayInfo();
		
		//setXY()
		point1.setXY(3, 3);
		System.out.println("Noile coordonate ale punctului 1:");
		point1.sayInfo();
		
		//distance
		System.out.println("Distanta de la punctul 1 la 4,3 "+point1.distance(4, 3));
		//overloaded distance
		System.out.println("Distanta dintre cele 2 puncte: "+point1.distance(point2));
	}
}
