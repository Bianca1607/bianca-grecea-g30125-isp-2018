package g30125.grecea.bianca.l3.e5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class Exercitiul5 {
	public static void main(String[] args)
	   {
	      // create the city
	      City ny = new City();
	      //create the walls
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve4 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve5 = new Wall(ny, 1, 2, Direction.SOUTH);
	      Wall blockAve6 = new Wall(ny, 2, 1, Direction.SOUTH);
	      //create the "newspaper"
	      Thing parcel = new Thing(ny, 2, 2);
	      //create karel
	      Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      //pick the "newspaper"
	      karel.pickThing();
	      //go back
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      //turn right
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      //put the "newspaper"
	      karel.putThing();
	   }
}
