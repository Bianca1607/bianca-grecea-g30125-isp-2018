package g30125.grecea.bianca.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Exercitiul4 {
	 public static void main(String[] args)
	   {
	      // create the city
	      City ny = new City();
	      //create the walls, 2 in each direction
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(ny, 1, 1, Direction.NORTH);
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve4 = new Wall(ny, 1, 2, Direction.EAST);
	      Wall blockAve5 = new Wall(ny, 2, 2, Direction.EAST);
	      Wall blockAve6 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Wall blockAve7 = new Wall(ny, 2, 2, Direction.SOUTH);
	      //create the robot as shown in the figure
	      Robot ann = new Robot(ny, 0, 2, Direction.WEST);
	      
	      //ann.turnRight(); not available
	      //turn around to go clockwise
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.move();
	      //turn 3 times left for the robot to go right
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.move();
	      ann.move();
	      ann.move();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.move();
	      ann.move();
	      ann.move();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.move();
	      ann.move();
	      ann.move();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.turnLeft();
	      ann.move();
	      ann.move();
	      //turn around to be in the initial situation
	      ann.turnLeft();
	      ann.turnLeft();
	   }
}
