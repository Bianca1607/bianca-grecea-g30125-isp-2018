package g30125.grecea.bianca.l3.e3;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class Exercitiul3 {
	public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	  City prague = new City();
	   	  //create robot that heads north
	      Robot R = new Robot(prague, 1, 1, Direction.NORTH);
	      //move 5 times to north
	      R.move();
	      R.move();
	      R.move();
	      R.move();
	      R.move();
	      //turn around
	      R.turnLeft();
	      R.turnLeft();
	      //go back
	      R.move();
	      R.move();
	      R.move();
	      R.move();
	      R.move();
	      //turn around to the initial situation
	      R.turnLeft();
	      R.turnLeft();
	   }
}
