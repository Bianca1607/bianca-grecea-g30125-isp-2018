package problemasuplimentare;

public class Main {

	    public static int solution(int[] A){
	       int ok=0;
	        for(int i=0;i<A.length;){
	        	if(A[i]!=0) {
	        		ok=0;
	        		for(int j=i+1;j<A.length;j++){
	    	        	if(A[j]!=0) {
	    	        		if(A[j]==A[i]) {
	    	        			A[i] = A[j] =0;
	    	        			ok=1;
	    	        			break;
	    	        		}
	    	        	}
		        	}
	        		if(ok==0)
		        		return A[i];
	        	}
	        	i++;
	        }
	        return 0;
	    }

	    public static void main(String[] args) {
	        int[] A = new int[7];
	        A[0] = 9;  A[1] = 3;  A[2] = 9;
	        A[3] = 3;  A[4] = 9;  A[5] = 7;
	        A[6] = 9;
	        int result = solution(A);

	        if(result==7)
	            System.out.println("Rezultat corect."+result);
	        else
	            System.out.println("Rezultat incorect.");
	    }

}
