package g30125.grecea.bianca.l10.e3;

public class Counter2 extends Thread {
	String n;
    Thread t;
    Counter2(String n, Thread t){this.n = n;this.t=t;}
	    
	    public void run(){
	    	int a,b;
	    	if(t==null) {
	    		a=0;
	    		b=100;
	    	}
	    	else {
	    		a=100;
	    		b=200;
	    	}
	          for(int i=a;i<b;i++){
	        	  try
	                {                
	                      if (t!=null) t.join();
	                      Thread.sleep(100);
	                   
	                } catch(Exception e) {
	                	e.printStackTrace();
	                	} 
	                System.out.println(getName() + " i = "+i);
	                }
	          System.out.println("Firul "+n+" a terminat operatia.");
	     
	    }

	    public static void main(String[] args) {
	    	Counter2 w1 = new Counter2("Proces 0",null);
	    	Counter2 w2 = new Counter2("Proces 1",w1);
	        w1.start();
	        w2.start();
	    }
	}
