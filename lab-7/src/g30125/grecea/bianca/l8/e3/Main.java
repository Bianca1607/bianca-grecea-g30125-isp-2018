package g30125.grecea.bianca.l8.e3;



import java.util.*;


import g30125.grecea.bianca.l8.e2.Bank;
import g30125.grecea.bianca.l8.e2.BankAccount;
import g30125.grecea.bianca.l8.e2.Comparable;

public class Main {
	 public static void main(String[] args) {
		//sortare dupa nume
			TreeSet<BankAccount> comp3 = new TreeSet<BankAccount>(new Comp());
	        comp3.add(new BankAccount("Ram",3000));
	        comp3.add(new BankAccount("John",6000));
	        comp3.add(new BankAccount("Crish",2000));
	        comp3.add(new BankAccount("Tom",2400));
	        for(BankAccount e:comp3){
	            System.out.println(e);
	        }
	        System.out.println("------");
	        //sortare dupa depozit
	        TreeSet<BankAccount> comp4 = new TreeSet<BankAccount>(new Comp2());
	        comp4.add(new BankAccount("Ram",3000));
	        comp4.add(new BankAccount("John",6000));
	        comp4.add(new BankAccount("Crish",2000));
	        comp4.add(new BankAccount("Tom",2400));
	        for(BankAccount e:comp4){
	            System.out.println(e);
	        }
	 }
	 
}


class Comp implements Comparator<BankAccount>{

	@Override
	public int compare(BankAccount arg0, BankAccount arg1) {
		String s0=arg0.getOwner();
		String s1=arg1.getOwner();
         return s0.compareToIgnoreCase(s1);
	}
}  

class Comp2 implements Comparator<BankAccount>{

	@Override
	public int compare(BankAccount arg0, BankAccount arg1) {
		if(arg0.getBalance()>arg1.getBalance()) return 1;
        if(arg0.getBalance()==arg1.getBalance()) return 0;
        return -1;
	}
} 
