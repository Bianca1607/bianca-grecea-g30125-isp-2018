package g30125.grecea.bianca.l8.e1;

public class BankAccount {
	private String owner;
	private double balance;
	
	BankAccount(String owner, double balance){
		this.owner=owner;
		this.balance=balance;
	}
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void withdraw(double amount) {
		double a;
		a=getBalance();
		if(a>amount) {
			a=a-amount;
			setBalance(a);
			System.out.println("Finish!");
		}
		else System.out.println("Not enough money!");
	}
	
	
	public void deposit(double amount) {
		double a;
		a=getBalance();
		a=a+amount;
		setBalance(a);
		System.out.println("Finish!");
	}
	 @Override
	    public boolean equals(Object o) {
	 
	        // If the object is compared with itself then return true  
	        if (o == this) {
	            return true;
	        }
	 
	        /* Check if o is an instance of Complex or not
	          "null instanceof [type]" also returns false */
	        if (!(o instanceof BankAccount)) {
	            return false;
	        }
	         
	
	        BankAccount c = (BankAccount) o;

	        return owner.compareTo(c.owner) == 0
	                && Double.compare(balance, c.balance) == 0;
	    }
	 
	 @Override
	    public int hashCode() {
	        double result = 17;
	        result = 31 * result + owner.hashCode();
	        result = 31 * result + balance;
	        return (int)result;
	    }
	 public static void main(String[] args) {
		 BankAccount b1=new BankAccount("A11", 25);
		 BankAccount b2=new BankAccount("A10", 25);
		 if(b1.equals(b2))
			 System.out.println("Equal");
		 else
			 System.out.println("Not equal");
		
	 }
}
