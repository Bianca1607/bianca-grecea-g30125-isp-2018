package g30125.grecea.bianca.l8.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Bank extends BankAccount{
	
	Bank(String owner, double balance) {
		super(owner, balance);
		// TODO Auto-generated constructor stub
	}
	
	private static ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();
	 
	public void addAccount(String owner, double balance) {
		BankAccount b1=new BankAccount(owner, balance);
		accounts.add(b1);
		}
	
	public static ArrayList<BankAccount> getAllAccounts() {
		return accounts;
	}

	public static void setAccounts(ArrayList<BankAccount> accounts) {
		Bank.accounts = accounts;
	}

	public void printAccounts() {
		Collections.sort(accounts, new Comparator<BankAccount>() {
			@Override
			public int compare(BankAccount arg0, BankAccount arg1) {
		         if(arg0.getBalance()>arg1.getBalance()) return 1;
		         if(arg0.getBalance()==arg1.getBalance()) return 0;
		         return -1; 
			}
        });
		Iterator i=accounts.iterator();
		while(i.hasNext()){
            BankAccount a = (BankAccount) i.next();
            	System.out.println(a.getOwner()+" \t Money:"+a.getBalance());
      }
	}
	
	public void printAccounts(double minBalance, double maxBalance) {
	        for (int i = 0; i < accounts.size(); i++) {
	            BankAccount a = (BankAccount) accounts.get(i);
	            if (a.getBalance() >= minBalance && a.getBalance() <= maxBalance)
	            	System.out.println(a.getOwner()+" \t Money:"+a.getBalance());
	        }
	}
	
	BankAccount getAccount(String owner) {
		Iterator i=accounts.iterator();
		while(i.hasNext()){
            BankAccount a = (BankAccount) i.next();
            if(owner.equals(a.getOwner()))
            	return a;
      }
		return null;
	}
	
}
