package g30125.grecea.bianca.l8.e2;

public interface Comparable {
	int compareTo(Object o);
}
