package g30125.grecea.bianca.l8.e2;

import java.util.*;

import g30125.grecea.bianca.l8.e2.BankAccount;
import g30125.grecea.bianca.l8.e2.Bank;
import g30125.grecea.bianca.l8.e2.Comparable;

public class Main {

	 public static void main(String[] args) {
		 Bank banca = new Bank("AA", 10);
		  ArrayList<BankAccount> b = banca.getAllAccounts();
		  
		 banca.addAccount("Bianca", 1000);
		 banca.addAccount("Bogdan", 10);
		 banca.addAccount("Andrei", 100);
		 banca.addAccount("Mihai", 9999);
		
		 System.out.println("Toate conturile(sortate):");
		 banca.printAccounts();
		 
		 System.out.println("-----\nConturile intre 2 si 200:");
		 banca.printAccounts(2, 200);
		 
		 BankAccount b1=banca.getAccount("Mihai");
		 System.out.println("-----\n"+b1.toString());
		 
		 System.out.println("-----\nSortare dupa nume:");
		 Collections.sort(b, new Comparator<BankAccount>() {
				@Override
				public int compare(BankAccount arg0, BankAccount arg1) {
					String s0=arg0.getOwner();
					String s1=arg1.getOwner();
			         return s0.compareToIgnoreCase(s1);
				}
	        });
			Iterator i=b.iterator();
			while(i.hasNext()){
	            BankAccount a = (BankAccount) i.next();
	            	System.out.println(a.getOwner()+" \t Money:"+a.getBalance());
	      }
	 }
	 
}
