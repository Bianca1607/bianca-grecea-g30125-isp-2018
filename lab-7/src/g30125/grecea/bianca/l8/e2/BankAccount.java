package g30125.grecea.bianca.l8.e2;

import java.util.Objects;


public class BankAccount implements Comparable{
	private String owner;
	private double balance;
	
	public BankAccount(String owner, double balance){
		this.owner=owner;
		this.balance=balance;
	}
	
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void withdraw(double amount) {
		double a;
		a=getBalance();
		if(a>amount) {
			a=a-amount;
			setBalance(a);
			System.out.println("Finish!");
		}
		else System.out.println("Not enough money!");
	}
	
	
	public void deposit(double amount) {
		double a;
		a=getBalance();
		a=a+amount;
		setBalance(a);
		System.out.println("Finish!");
	}
	 @Override
	    public boolean equals(Object o) {
	        if (o == this) 
	            return true;
	        if (!(o instanceof BankAccount))
	            return false;
	        BankAccount c = (BankAccount) o;
	        return owner.compareTo(c.owner) == 0
	                && Double.compare(balance, c.balance) == 0;
	    }
	 
	 @Override
	    public int hashCode() {
		 return Objects.hash(owner, balance);
	    }
	 
	 @Override
	 public int compareTo(Object o) {
         BankAccount p = (BankAccount)o;
         if(balance>p.balance) return 1;
         if(balance==p.balance) return 0;
         return -1; 
   }
	
	 public String toString() {
	        return "BankAccount{" +
	                "name='" + owner + '\'' +
	                ", balance=" + balance +
	                '}';
	    }
}
