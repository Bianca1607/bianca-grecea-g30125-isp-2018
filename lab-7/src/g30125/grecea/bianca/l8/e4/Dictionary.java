package g30125.grecea.bianca.l8.e4;


import java.util.HashMap;
import java.util.Map;

public class Dictionary {
	HashMap<Word, Definition> map;
	public Dictionary(HashMap map) {
		this.map=map;
	}
	public void addWord(Word w, Definition d) {
		map.put(w, d);
	}
	public Definition getDefinition(Word w) {
		return (Definition)map.get(w);
}
	public void getAllWords() {
		for(Map.Entry<Word, Definition> entry:map.entrySet()) {
			Word key= entry.getKey();
			System.out.println(key.getName());
		}
}
	public void getAllDefinitions() {
		for(Map.Entry<Word, Definition> entry:map.entrySet()) {
			Definition value=entry.getValue();
			System.out.println(value.getDescription());
		}
	}
}