package g30125.grecea.bianca.l5.e1;

import g30125.grecea.bianca.l5.e1.Shape;

public class Test {
	public static void main(String[] args) {
		Shape[] A = new Shape[2];
		
		//Circle
		A[0] = new Circle("red",true,2);
		System.out.println("Circle's Area is: "+A[0].getArea());
		System.out.println("Circle's Perimeter is: "+A[0].getPerimeter());
		System.out.println(A[0].toString());
		
		
		//Rectangle
		A[1] = new Rectangle("green", false,2,4);
		System.out.println("Rectangle's Area is: "+A[1].getArea());
		System.out.println("Rectangle's Perimeter is: "+A[1].getPerimeter());
		System.out.println(A[1].toString());
	}
}
