package g30125.grecea.bianca.l5.e2;

public class Test {
	public static void main(String[] args) {
		System.out.println("---Should display:Loading Real");
		RealImage i=new RealImage("Real");
		System.out.println("---Should display: Displaying Real");
		i.display();
		ProxyImage ii=new ProxyImage("Proxy",3);
		System.out.println("---Should display according to an option");
		ii.display();
	}
}
