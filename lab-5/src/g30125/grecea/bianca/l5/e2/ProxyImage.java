package g30125.grecea.bianca.l5.e2;

public class ProxyImage implements Image{
	 
	   private Image image;
	   private String fileName;
	 
	   public ProxyImage(String fileName, int option){
		   if(option==1) {
			   image=new RealImage(fileName);
		   }
		   else
			   image=new RotatedImage(fileName); 
	      this.fileName = fileName;
	   }
	 
	   @Override
	   public void display() {
	      image.display();
	   }
	}