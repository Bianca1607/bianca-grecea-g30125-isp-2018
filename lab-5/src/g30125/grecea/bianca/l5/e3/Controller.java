package g30125.grecea.bianca.l5.e3;

import java.util.Date;

public class Controller {
	public LightSensor lightSensor;
	public TemperatureSensor temperatureSensor;
	
	public Controller(LightSensor lightSensor, TemperatureSensor temperatureSensor) {
		System.out.println("Start Control!");
		this.lightSensor=lightSensor;
		this.temperatureSensor=temperatureSensor;
	}
	
	//control() method will read and display temperature and light values
	//with a period of 1 second for duration of 20 seconds;
	public void control() {
		int i=0;
		while(i<20) {
			System.out.println("Temperature Value: "+temperatureSensor.readValue());
			System.out.println("Light Value: "+lightSensor.readValue());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
	}
}
