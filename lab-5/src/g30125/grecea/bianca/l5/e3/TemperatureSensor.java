package g30125.grecea.bianca.l5.e3;

class TemperatureSensor extends Sensor {

	public TemperatureSensor() {
		System.out.println("Temperature Sensor");
	}
	
	@Override
	public int readValue() {
		int j=(int)(Math.random()*101);
		return j;
	}
}
