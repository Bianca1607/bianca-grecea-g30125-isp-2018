package g30125.grecea.bianca.l5.e3;

public class Test {
	public static void main(String[] args) {
		TemperatureSensor t = new TemperatureSensor();
		LightSensor l = new LightSensor();
		Controller c = new Controller(l,t);
		c.control();
	}
}
