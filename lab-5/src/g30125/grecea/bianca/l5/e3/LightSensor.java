package g30125.grecea.bianca.l5.e3;

class LightSensor extends Sensor {
	public LightSensor() {
		System.out.println("Light Sensor");
	}
	
	@Override
	public int readValue() {
		int j = (int)(Math.random()*101);
		return j;
	}
}
