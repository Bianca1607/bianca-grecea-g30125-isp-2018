package g30125.grecea.bianca.l5.e4;

import g30125.grecea.bianca.l5.e3.Sensor;

class LightSensor extends Sensor {
	public LightSensor() {
		System.out.print("Light Sensor");
	}
	
	@Override
	public int readValue() {
		int j = (int)(Math.random()*101);
		return j;
	}
}
