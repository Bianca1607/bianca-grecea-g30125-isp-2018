package g30125.grecea.bianca.l5.e4;

public class Controller {
    private static volatile Controller instance = null;
    Controller() {
    	System.out.println("Start Control!");
    }
 
    public static Controller getInstance() {
    synchronized (Controller.class) {
                if (instance == null) {
                    instance = new Controller();
                }
            	//control() method will read and display temperature and light values
            	//with a period of 1 second for duration of 20 seconds;
            		int i=1;
            		LightSensor lightSensor=new LightSensor();
            		System.out.println(" activated!");
            		TemperatureSensor temperatureSensor= new TemperatureSensor();
            		System.out.println(" activated!");
            		while(i<=20) {
            			System.out.print("Value no: "+i+"\tTemperature's Value: "+temperatureSensor.readValue());
            			System.out.println("\tLight's Value: "+lightSensor.readValue());
            			try {
            				Thread.sleep(1000);
            			} catch (InterruptedException e) {
            				// TODO Auto-generated catch block
            				e.printStackTrace();
            			}
            			i++;
            	}
            		System.out.println("Stop Control!");
            }
 
        return instance;
    }
}
