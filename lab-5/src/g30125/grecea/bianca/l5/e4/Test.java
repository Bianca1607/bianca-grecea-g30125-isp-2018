package g30125.grecea.bianca.l5.e4;

public class Test {
	public static void main(String[] args) {
		TemperatureSensor t = new TemperatureSensor();
		System.out.println("\tFirst attempt: "+t.readValue());
		LightSensor l = new LightSensor();
		System.out.println("\t        First attempt: "+l.readValue());
		Controller c = new Controller();
		System.out.println("The values for 20 seconds!");
		c.getInstance();
	}
}
