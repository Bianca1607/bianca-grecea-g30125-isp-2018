package g30125.grecea.bianca.l5.e4;

import g30125.grecea.bianca.l5.e3.Sensor;

class TemperatureSensor extends Sensor {

	public TemperatureSensor() {
		System.out.print("Temperature Sensor");
	}
	
	@Override
	public int readValue() {
		int j=(int)(Math.random()*101);
		return j;
	}
}
