package g30125.grecea.bianca.l9.e3;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.*;
import java.nio.file.*;

import javax.swing.*;

import g30125.grecea.bianca.l9.e1.ButtonAndTextField2;

import java.util.*;

public class FileTextButton extends JFrame {

	 JLabel numar;
	 JTextField tnumar;
     JTextArea tArea;
     JButton buton;
    
    FileTextButton(int nr){
        setTitle("Test deschidere fisier");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
  }

  public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        numar = new JLabel("Enter name:");
        numar.setBounds(10, 50, width+50, height);
        
        tnumar = new JTextField();
        tnumar.setBounds(80,50,width, height);

        buton = new JButton("Show!");
        buton.setBounds(10,150,width, height);
        buton.addActionListener(new TratareButonCounter());
        
        tArea = new JTextArea();
        tArea.setBounds(100,100,width+40, height+40);

        add(numar);
        add(buton);
        add(tArea);
        add(tnumar);

  }

  public static void main(String[] args) {
        new FileTextButton(0);
  }

  class TratareButonCounter implements ActionListener{
	  public String readFileAsString(String fileName)throws Exception
	    {
	        String data = "";
	        data = new String(Files.readAllBytes(Paths.get(fileName)));
	        return data;
	    }
        public void actionPerformed(ActionEvent e) {
        	
        	try{
        		String data = readFileAsString(FileTextButton.this.tnumar.getText());
            	  FileTextButton.this.tArea.setText("");
            	  FileTextButton.this.tArea.append(data);
        }
        	catch(Exception e2) {
        		System.out.println("exceptie:"+e2.getMessage());
        	}
        	 finally {
 	        }
  }
       
}

  
}