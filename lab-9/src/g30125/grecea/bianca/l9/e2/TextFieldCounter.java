package g30125.grecea.bianca.l9.e2;


import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class TextFieldCounter extends JFrame {


	 JLabel numar;
     JTextArea tArea;
     JButton buton;
    
     TextFieldCounter(int nr){
        setTitle("Test counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init(nr);
        setSize(200,300);
        setVisible(true);
  }

  public void init(int nr){

        this.setLayout(null);
        int width=80;int height = 20;

        numar = new JLabel("Counter");
        numar.setBounds(10, 50, width, height);

        buton = new JButton("Up!");
        buton.setBounds(10,150,width, height);
        buton.addActionListener(new TratareButonCounter());
        
        tArea = new JTextArea();
        tArea.setBounds(70,50,width, height);

        add(numar);
        add(buton);
        add(tArea);

  }

  public static void main(String[] args) {
        new TextFieldCounter(0);
  }

  class TratareButonCounter implements ActionListener{
	  int nr=0;
        public void actionPerformed(ActionEvent e) {
        	TextFieldCounter.this.tArea.setText("");
        	TextFieldCounter.this.tArea.append("nr:"+nr);
            	  nr++;
        }
  }
}