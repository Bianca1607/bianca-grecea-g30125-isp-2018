package g30125.grecea.bianca.l4.e8;

public class Shape {
	String color;
	boolean filled;

	public Shape() {
		color = "red";
		filled = true;
	}

	public Shape(String color, boolean filled) {
		this.color = color;
		this.filled = filled;
	}

	public String getcolor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isfilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public String toString() {
		return " A Shape with color " + this.color + " and filled " + this.filled;
	}
}
