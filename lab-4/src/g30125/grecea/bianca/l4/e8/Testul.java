package g30125.grecea.bianca.l4.e8;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Testul{
	@Test
	public void testCircleArea() {
		Circle c1 = new Circle();
		assertEquals(3.141592653589793,c1.getArea(),0.01);
	}
	@Test
	public void testCirclePerimter() {
		Circle c1 = new Circle();
		assertEquals(6.283185307179586,c1.getPerimeter(),0.01);
	}
	@Test
	public void testRectangleArea() 
	{
		Rectangle rectangle = new Rectangle();
		assertEquals(1.0,rectangle.getArea(),0.01);
	}
	@Test
	public void testSquareSide() 
	{
		Square square = new Square(15.0);
		assertEquals(15.0,square.getSide(),0.01);
	}
}
