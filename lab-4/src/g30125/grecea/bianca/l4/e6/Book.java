package g30125.grecea.bianca.l4.e6;

import g30125.grecea.bianca.l4.e4.Author;


/*You shall re-use the Author class written earlier.*/
public class Book {

	
	private String name;
	private Author[] author;
	private double price;
	private int qtyInStock=0;
	
	
	/*The constructors take an array of Author (i.e., Author[]), instead of an Author instance.*/
	public Book (String name, Author[] author, double price) {
		this.name=name;
		this.author=author;
		this.price=price;
	}
	public Book (String name, Author[] author, double price,int qtyInStock) {
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	
	
	/*public methods getName(), getAuthor(), getPrice(), setPrice(), getQtyInStock(), setQtyInStock().*/
	public String getName() {
		return name;
	}
	public Author[] getAuthor() {
		return author;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	
	/*The toString() method shall return �book-name by n authors�, where n is the number of authors.*/
	public String toString() {
		return "'"+name+"' by "+author.length+" authors";
	}
	
	
	/*A new method printAuthors() to print the names of all the authors.*/
	public void printAuthors() {
		for(int i=0;i<author.length;i++) {
			System.out.println("Author "+i+": "+author[i].getName());
		}
	}
	public static void main(String[] args) {
		//main adaugat pentru verificarea corectitudinii datelor(este optional)
		Author[] author = new Author[2];
		author[0]=new Author("J.D. Salinger","jerome.salinger@gmail.com",'M');
		author[1]=new Author("Sarah Graham","sarah.graham@gmail.com",'F');
		Book book = new Book("The Catcher in the Rye",author,42.99);
		System.out.println(book.toString());
		book.printAuthors();
	}

}
