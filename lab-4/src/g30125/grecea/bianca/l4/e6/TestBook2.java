package g30125.grecea.bianca.l4.e6;

import g30125.grecea.bianca.l4.e4.Author;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestBook2 {
	@Test
	public void shouldPrint() {
		Author[] author = new Author[2];
		author[0]=new Author("J.D. Salinger","jerome.salinger@gmail.com",'M');
		author[0]=new Author("Sarah Graham","sarah.graham@gmail.com",'F');
		Book book = new Book("The Catcher in the Rye",author,42.99);
		assertEquals(" '"+book.getName()+"' by "+author.length+" authors",book.toString());
	}
}
