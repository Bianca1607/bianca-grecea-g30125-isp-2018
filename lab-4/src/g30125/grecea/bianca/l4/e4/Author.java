package g30125.grecea.bianca.l4.e4;

public class Author {
	
	//Three private instance variables: name (String), email (String), and gender (char of either 'm' or 'f');
	private final String name;
	private String email;
	private final char gender;
	
	
	//One constructor to initialize the name, email and gender with the given values;
	//public Author (String name, String email, char gender) {��}
	public Author(String name1, String email1, char gender1) {
		name=name1;
		email=email1;
		gender=gender1;
	}
	
	
	//(There is no default constructor for Author, as there are no defaults for name, email and gender.)
	//public getters/setters: getName(), getEmail(), setEmail(), and getGender();
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public char getGender() {
		return gender;
	}
	
	
	//(There are no setters for name and gender, as these attributes cannot be changed.)
	//A toString() method that returns �author-name (gender) at email�
	 public String toString(){
	        return "Author "+name+"("+gender+") at email: "+email;
	    }
}
