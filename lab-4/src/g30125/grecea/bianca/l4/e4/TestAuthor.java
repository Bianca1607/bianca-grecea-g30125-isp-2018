package g30125.grecea.bianca.l4.e4;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestAuthor {
	@Test
    public void shouldAddAuthor(){
        Author a1 = new Author("Bianca","bianca.yahoo.com",'f');
        assertEquals("Bianca",a1.getName());//primul e rezultatul asteptat
    }
	@Test
    public void shouldPrintAuthor(){
        Author a2 = new Author("Elena","elena.yahoo.com",'f');
        assertEquals("Author "+a2.getName()+"("+a2.getGender()+") at email: "+a2.getEmail(),a2.toString());
    }
}
