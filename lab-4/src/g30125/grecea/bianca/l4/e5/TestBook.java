package g30125.grecea.bianca.l4.e5;

import g30125.grecea.bianca.l4.e4.Author;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestBook {
	@Test
	public void shouldPrint() {
		Author author = new Author("J.D. Salinger","jerome.salinger@gmail.com",'M');
		Book book = new Book("The Catcher in the Rye",author,42.99);
		assertEquals(" '"+book.getName()+"' by "+author.getName()+" ("+author.getGender()+") at email: "+author.getEmail(),book.toString());
	}
}
