package g30125.grecea.bianca.l4.e7;


public class Circle {
	//Two private instance variables: radius (of type double) and color (of type String), with default value of 1.0 and �red�, respectively
	private double radius;
	private String color;
	//Two overloaded constructors;
	public Circle(){
		radius=1.0;
		color="red";
	}
	public Circle(double radius){
		this.radius=radius;
		color="red";
	}
	//Two public methods: getRadius() and getArea().
	public double getRadius() {
		return radius;
	}
	public double getArea() {
		return 3.14*radius;
	}
	public static class Cylinder extends Circle {

		private double height;
		Cylinder() {
			height=1.0;
			System.out.println("Constructor cilindru 1");
		}
		Cylinder(double radius) {
			super(radius);
			System.out.println("Constructor cilindru 2");
		}
		Cylinder(double radius,double height) {
			super(radius);
			this.height=height;
			System.out.println("Constructor cilindru 3");
		}
		public double getHeight() {
			return height;
		}
		public double getVolume() {
			return height*Math.PI*Math.pow(getRadius(), 2);
		}
		public double getArea() {
			return 2*Math.PI*getRadius()*(getRadius()+height);
		}
	}

}


	
