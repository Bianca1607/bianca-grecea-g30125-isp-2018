package g30125.grecea.bianca.l4.e7;

import g30125.grecea.bianca.l4.e7.Circle.Cylinder;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestCylinder {
	@Test
	public void testArea() {
		Cylinder c1=new Cylinder(3.0,4.0);
		assertEquals(131.94689145077132,c1.getArea(),0.01);
	}
	@Test
	public void testVolume() {
		Cylinder c2=new Cylinder(3.0,4.0);
		assertEquals(113.09733552923255,c2.getVolume(),0.01);
	}
}
