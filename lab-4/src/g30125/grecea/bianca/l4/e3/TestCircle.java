package g30125.grecea.bianca.l4.e3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestCircle {
	@Test
    public void shouldAddCircle(){
        Circle c1 = new Circle();
        assertEquals(1.0,c1.getRadius(),0.01);
    }
	@Test
    public void shouldAddCircle2(){
        Circle c2 = new Circle(2);
        assertEquals(2.0,c2.getRadius(),0.01);
    }
	@Test
    public void shouldArea(){
        Circle c = new Circle();
        assertEquals(c.getRadius()*3.14,c.getArea(),0.01);
    }
}
