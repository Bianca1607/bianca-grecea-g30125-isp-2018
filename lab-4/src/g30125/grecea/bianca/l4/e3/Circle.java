package g30125.grecea.bianca.l4.e3;

public class Circle {
	//Two private instance variables: radius (of type double) and color (of type String), with default value of 1.0 and �red�, respectively
	private double radius;
	private String color;
	//Two overloaded constructors;
	public Circle(){
		radius=1.0;
		color="red";
	}
	public Circle(double radius){
		this.radius=radius;
		color="red";
	}
	//Two public methods: getRadius() and getArea().
	public double getRadius() {
		return radius;
	}
	public double getArea() {
		return 3.14*radius;
	}
	
}
