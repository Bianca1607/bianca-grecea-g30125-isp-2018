package g30125.grecea.bianca.l8.e2;

import java.io.*;
import java.nio.file.*;

public class CountLetter {

	    public static String readFileAsString(String fileName)throws Exception
	    {
	        String data = "";
	        data = new String(Files.readAllBytes(Paths.get(fileName)));
	        return data;
	    }

	    public static void main(String[] args) throws Exception
	    {
	        try{
	            int counter=0;
	            String data = readFileAsString("C:\\Users\\Bianca\\Desktop\\fisier.txt");
	            for(int i = 0; i< data.length();i++)
	            {
	                if( data.charAt(i) == 'a' )
	                    counter++;
	            }
	            System.out.println("The character 'a' appears "+counter+" times");
	        }
	        finally {
	        }
	    }
}