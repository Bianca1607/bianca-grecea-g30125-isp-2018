package g30125.grecea.bianca.l6.e3;
import java.awt.Color;


public class Main {
	public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED,60,40,"C1",90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN,50,70,"C2", 100);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,150,50,"R1",100,70);
        b1.addShape(s3);
        //b1.deleteById("C1");
    }
}
