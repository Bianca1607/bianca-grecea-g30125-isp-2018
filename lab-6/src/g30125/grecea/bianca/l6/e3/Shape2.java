package g30125.grecea.bianca.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Shape2 implements Shape {
	 private Color color;
	    public String id;
	    private boolean fill=true;


	   public Shape2(Color color, String id, boolean fill) {
	        this.color = color;
	        this.id=id;
	        this.fill=fill;
	    }

		 public String getId() {
				return id;
			}
	    public Color getColor() {
	        return color;
	    }

	    public void setColor(Color color) {
	        this.color = color;
	    }

	    public boolean isFill() {
			return fill;
		}

		public void setFill(boolean fill) {
			this.fill = fill;
		}

		public abstract void draw(Graphics g);
}
