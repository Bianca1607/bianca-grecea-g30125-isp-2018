package g30125.grecea.bianca.l6.e3;
import java.awt.Color;
import java.awt.Graphics;

public interface Shape {


	 public String getId() ;

    public Color getColor();

    public void setColor(Color color);
    
    public boolean isFill() ;

	public void setFill(boolean fill) ;
	public abstract void draw(Graphics g);
}
