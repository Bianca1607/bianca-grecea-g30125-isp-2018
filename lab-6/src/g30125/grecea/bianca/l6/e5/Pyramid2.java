package g30125.grecea.bianca.l6.e5;
import java.awt.*;

import javax.swing.*;

public class Pyramid2
{
   public static void main(String[] args)
   {
        JFrame frame=new JFrame();
        frame.setSize(1000,500);
        frame.setContentPane(new Pyramid());
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}