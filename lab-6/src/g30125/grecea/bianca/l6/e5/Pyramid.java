package g30125.grecea.bianca.l6.e5;

import java.awt.*;

import javax.swing.*;

class Pyramid extends JPanel
{

    public void paint(Graphics g)
    {
        int BRICKS_IN_BASE,width,height;
        width=30;
        height=12;
        BRICKS_IN_BASE=14;
        for (int i = 0; i <= BRICKS_IN_BASE; i++)
        {
            for (int j = 1; j <= (BRICKS_IN_BASE - i)/2; j++)
            {
                g.setColor(Color.white);
                g.drawRect(width*i,height*i,width,height);
            }
            // for (int k=1; k<=2star*i -1
            for (int k = 1; k <= i; k++)
            {
                g.setColor(Color.black);
                g.drawRect(width/2,height/2,width,height);
            }
        }
    }
}

