package g30125.grecea.bianca.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;
    private int x,y;

    public Rectangle(Color color,int x, int y, String id, int length, int width) {
        super(color,id,true);
        this.length = length;
        this.width=width;
        this.x=x;
        this.y=y;
    }  

    public int getLength() {
		return length;
	}


	public int getWidth() {
		return width;
	}

	@Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+this.length+" "+this.width+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect (x, y, length, width); 
    }
}