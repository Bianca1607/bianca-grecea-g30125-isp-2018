package g30125.grecea.bianca.l6.e1;



import java.awt.*;

public class Circle extends Shape{

    private int radius; 
    private int x, y;

    public Circle(Color color,int x, int y, String id, int radius) {
        super(color,id,true);
        this.x=x;
        this.y=y;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(x,y,radius,radius);
    }
}