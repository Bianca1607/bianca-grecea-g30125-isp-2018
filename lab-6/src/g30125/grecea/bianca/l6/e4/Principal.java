package g30125.grecea.bianca.l6.e4;

public class Principal {
	public static void main(String[] args) {
		ImplementCharSequence implementCharSequence=new ImplementCharSequence(new char[] {'b','i','a','n','c','a'});
		for(int i=0;i<implementCharSequence.length();i++)
		{
			System.out.println("Char at "+i+" = "+implementCharSequence.charAt(i));
		}
		System.out.println(implementCharSequence.toString());
		System.out.println(implementCharSequence.subSequence(0, 2));
}
}
