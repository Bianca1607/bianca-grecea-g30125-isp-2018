package g31025.grecea.bianca.l2.e2;

public class Exercitiul2 {
	public static void main(String[] args) {
		int numar;
		numar=2;
		System.out.println("Varinta cu switch-case: ");
		switch(numar) {
		case 1:
			System.out.println("Unu");
			break;
		case 2:
			System.out.println("Doi");
			break;
		case 3:
			System.out.println("Trei");
			break;
		case 4:
			System.out.println("Patru");
			break;
		case 5:
			System.out.println("Cinci");
			break;
		case 6:
			System.out.println("Sase");
			break;
		case 7:
			System.out.println("Sapte");
			break;
		case 8:
			System.out.println("Opt");
			break;
		case 9:
			System.out.println("Noua");
			break;
			default:
				System.out.println("Numarul nu e de la 1-9!");
		}
		
		
		System.out.println("Varinta cu nested-if: ");
		if(numar == 1) {
			System.out.println("Unu");
		}else{
			if(numar == 2){
				System.out.println("Doi");
		}else {
			if(numar == 3){
				System.out.println("Trei");
		}else {
			if(numar == 4){
				System.out.println("Patru");
		}else {
			if(numar == 5){
				System.out.println("Cinci");
		}else {
			if(numar == 6){
				System.out.println("Sase");
		}else {
			if(numar == 7){
				System.out.println("Sapte");
		}else {
			if(numar == 8){
				System.out.println("Opt");
		}else {
			if(numar == 9){
				System.out.println("Noua");
		}else {
				System.out.println("Numarul nu e intre 1-9!");
		}
		}
		}	
		}
		}
		}
		}
		}
		}
	}

}
