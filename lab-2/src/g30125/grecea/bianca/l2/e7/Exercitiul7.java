package g31025.grecea.bianca.l2.e7;
import java.util.*;
public class Exercitiul7 {
	public static void main(String[] args) {
		System.out.println("Guess the number!");
		Scanner intrare = new Scanner(System.in);
		Random numar=new Random();
		int n=1;
		int numarbun;
		int guess=0;
		while(n!=2) {
			System.out.println("1.Play \t2.Exit");
			n=intrare.nextInt();
			switch(n) {
			case 1:
				numarbun=numar.nextInt(10);
				System.out.println("You only get 3 retries");
				for(int i=1;i<=3;i++) {
					System.out.print("Choice number "+i+" ");
					guess=intrare.nextInt();
					if(guess==numarbun) {
						System.out.println("Correct!");
						break;
					}
					else {
						if(guess<numarbun)
							System.out.println("Wrong answer, your number is too low");
						else
							System.out.println("Wrong answer, your number is too high");
					}
				}
				if(guess!=numarbun)
					System.out.println("You lost!");
				break;
			case 2:
				System.out.println("Goodbye!");
				break;
			default:
					System.out.println("Invalid choice!");
			}
		}
		intrare.close();
	}
}
