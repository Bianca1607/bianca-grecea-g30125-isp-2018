package g31025.grecea.bianca.l2.e6;
import java.util.*;

public class Exercitiul6 {
	static int f1(int i,int numarul) {
		if(i<=numarul)
			return i*f1(i+1,numarul);
		else return 1;
	}
public static void main(String[] args) {
	System.out.println("Dati numarul: ");
	Scanner intrare=new Scanner(System.in);
	int n = intrare.nextInt();
	
	
	//nerecursiv
	int factorial = 1;
	for(int i=1;i<=n;i++)
		factorial*=i;
	System.out.println(n+" factorial este egal cu "+factorial);
	
	//recursiv
	factorial=f1(1,n);
	System.out.println(n+" factorial este egal cu "+factorial);
	intrare.close();
}
}
